#!/usr/bin/env python3

from pydotplus.graphviz import graph_from_dot_file
from z3 import Real, Bool, SolverFor, parse_smt2_string
from z3 import Not, And, Or

import math
from expressions import Parser

abcd = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', "i", "j", "k", "l", "m", "n"]
nbits = ''
g_var = ''
vard, inpd, outd, alld, tuned = {}, {}, {}, {}, {}

def edge_encode(var) :
    temp2 = var.obj_dict['attributes']['guard']
    temp2 = temp2.replace('"','')
    temp2 = temp2.replace('y','x[i-1]')
    temp2 = temp2.replace('t','t[i-1]')
    temp2 = temp2.replace('&&',',')
    temp2 = temp2.replace('DOWN == 0.0','Not(DOWN[i])')
    temp2 = temp2.replace('DOWN == 1.0','DOWN[i]')
    temp2 = temp2.replace('UP == 1.0','UP[i]')
    temp2 = temp2.replace('UP == 0.0','Not(UP[i])')
    temp2 = temp2.replace('UP == 1.0','UP[i]')

    print("\n temp2 obtained after split=",temp2)
    temp3 = var.obj_dict['attributes']['updates']
    temp3 = temp3.replace('"','')
    temp3 = temp3.split(',')
    temp4= ''
    for i in temp3 :
        i = i.split('=')
        if i[0].strip() == i[1].strip() :
            temp4 += i[0].strip() + '[i]==' +i[1].strip() + '[i-1]' + ','
        elif i[0].strip() != i[1].strip() :
            temp4 += i[0].strip() + '[i]==' +i[1].strip()  + ','

    temp2 += ',' + temp4	
    print("\n edge_encode= ",temp2)
    return temp2


def bit_check(no, bit_position):
    if (no >> bit_position & 1):
        return ""
    else:
        return "Not"

def getattr(sg, tosearch, n):
    try:
        return n.obj_dict['attributes'][tosearch].strip('"').strip()
    except KeyError:
        return None


def dicts(sg, tosearch, k):
    ret = dict()
    try:
        for v in sg.obj_dict['attributes'][tosearch].split(','):
            vars = list()
            v = v.strip('"').strip()
            g_var = v
            vars += [Real(v+'_%s' % i)
                     for i in range(k+1)]
            ret[v] = vars
    except KeyError:
        pass

    return ret

def gen_smt(f=None, k=10, TIMEOUT=1000):
    f_write = open('z3file1.py','w')
    # Initialize the parser
    parser = Parser()

    # initialize the solver
    s = SolverFor('QF_NRA')

    # Now input the d
    d = Real('d')
    s.add(d > 0)
    f_write.write("from z3 import *\n")
    f_write.write("from sys import *\n")
    f_write.write("\nfrom math import *")
    f_write.write("\nimport networkx as nx ")

    f_write.write("\nff, fd ,d = Real('ff'), Real('fd'),Real('d')")
    # Now add the smt for each HA
    G = graph_from_dot_file(f)
    ret_dict = {}
    inputs= ''
    variables= ''
    for sg in G.get_subgraphs():
        print(sg.get_attributes())
        ha_name = sg.obj_dict['attributes']['label'].strip('"')
#
#        # The internal continuous vars
        vard = dicts(sg, 'vars', k)
#
#        # Now the inputs
        inpd = dicts(sg, 'inputs', k)
        print(inpd)
#
#        # Now the outputs
        outd = dicts(sg, 'outputs', k)
        print(outd)
#
#        # The dictionary with all dicts
        alld.update(vard)
        alld.update(outd)
        alld.update(inpd)
        alld.update(tuned)

        #  Get the number of bits needed to represent the states in
        # the HA.
        initial_loc = sg.obj_dict['attributes']['initial_loc'].strip('"')
        initial_loc.strip()
        nodes = sg.get_nodes()
        print("\n number of nodes=",nodes)
        nbits = math.ceil(math.log2(len(nodes))) if len(nodes) > 1 else 1
        print("\n number of bits=",nbits)
        # Initialize the variables for state encoding
        nbitvars = [[Bool(ha_name+'_'+str(i)+'_%s' % j)
                     for j in range(k+1)]
                    for i in range(nbits)]
        print("\n nbitvars=",nbitvars)
        fstring = "{0:0"+str(nbits)+"b}"
        print("\n fstring=",fstring)
        temp = ''
        # TODO: Now encode the HA
        edges = sg.get_edges()
        print("edges=",edges)
        print("nodes:", range(len(nodes)),len(nodes),nodes[0])
        for index,i in enumerate(nodes):
            print("\n node i=",index,i)
            node_ors = []
            for x in range(0, len(nbitvars)):
                temp += abcd[x]+","
            print("\n Source Node:",i.get_name(),i.get_attributes())
            try:
                inputs = sg.obj_dict['attributes']['inputs'].strip('"')
                variables = sg.obj_dict['attributes']['vars'].strip('"')
            except:
                print("\n Either no inputs or variables in Graph")                 
            print("def ", i.get_name(), "(", "a","b", "i,edge_data):")
            f_write.write("\ndef " + i.get_name() + "("+ "i,"+ temp+ " d ," + inputs+"," +variables + "): \n")
            f_write.write("     return(Or( \n")
            print("\n i.get_name():",i.get_name(),"attributes",i.get_attributes())
            node_label_value =  ''
            node_eqs = ' '
            node_invariant = ' '
            m =  len(nbitvars)
            print("\n m= ",m)
            temp = ' '
            comma = ''
            dest = ''
            deq = ''
            for index1,n in enumerate(nodes):
               

                n_dests = [(e.get_destination(),
                            e.obj_dict['attributes']['guard'].strip('"'),
                            e.obj_dict['attributes']['updates'].strip('"'))
                           for e in edges
                           if e.get_source() == i.get_name()]
                
                print("dest:",i.get_name(), '-->', n_dests)
                edgelist = ''
                print("dest",dest)
                if index == index1:
                    node_items = i.get_attributes()
                    for key,value in node_items.items():
                        print("keyvalue",key,":",value)
                        if key == 'label':
                            node_label_value = value
                            print("\n label_value=",node_label_value)
                        elif key == 'eqs':
                            node_eqs = value
                        elif key == 'invariant':
                    
                            node_invariant = value
                    
                temp = ' '



                for e in edges:
                    if (e.get_source() == i.get_name() and e.get_destination() == n.get_name()):
                        dest = e.get_destination()
                        print("\n e.get_source(): ",e.get_source(),"e.get_destination:",e.get_destination(),"n.get_name():",n.get_name())
                        for x in range(m):
                            temp += bit_check(index, m-x-1) + "(" + abcd[x] + "[i-1]),"
                        for x in range(m):
                            temp += bit_check(index1, m-x-1) + "(" + abcd[x] + "[i]),"
                        print("\n temp = ",temp ,"index = ",index,"index1=",index1)
                        guards = edge_encode(e)
                        updates = e.obj_dict['attributes']['updates'].strip('"')
                        upd  = updates.strip("=")
                        print("\n upd =",upd[0],upd[4])
                        print("\n guards= ",guards, "updates= ",updates,"stripped",upd[0],"[i]",upd[0],"[i-1]")
                        print("\n   And(",  temp  ,",", guards ,",",updates, "),")
                        dest_node_name = n.get_name()
                        dest_node_attr = n.get_attributes()
                        node_items = i.get_attributes()
                        print ("\n dest_node_name =  ",dest_node_name )
                        #encoding differential equations
                        for key,value in dest_node_attr.items():
                            print("keyvalue",key,":",value)
                            if key == 'odes':
                                node_label_value = value
                                print("\n odes=",node_label_value)
                                temp2 = node_label_value.split('=')
                                print("\n temp2[0] = ",temp2[0],"temp2[1] = ",temp2[1])
                                temp3 = temp2[0].split('"')
                                temp4 = temp2[1].split('"')
                                print("\n temp4[0] = ",temp4[0] ,"temp4[1]=", temp4[1])
                                print("\n temp3[0] temp3[1] =",temp3[0],temp3[1])
                                if('der(x)' in temp3[1]):
                                    try :
                                        ind = temp2[1].index('x')
                                    except:
                                         print("\n ind = ", ind)
                                    temp2[0] = 'x[i] =='
                                    if(ind != -1) :
                                        rh = temp4[0].replace('x','x[i-1]')
                                        rh = 'x[i-1] +' +'('+ rh+ ' )*d '
                                        print("\n rh =",rh)
                                        temp2[1] = rh
                                        deq = temp2[0]+rh
                                        #f_write.write("   And("+  temp  + guards +","+upd[0]+"[i]="+upd[4]+"[i-1]"+ temp2[0]+rh + ","+  "),\n")
                                    else :
                                        deq = temp2
                                        #f_write.write("   And("+  temp  + guards +","+upd[0]+"[i]="+upd[4]+"[i-1]"+ temp2 +   "),\n")
                                else:
                                    deq = node_label_value
                                    #f_write.write("   And("+  temp  + guards +","+upd[0]+"[i]="+upd[4]+"[i-1]"+ node_label_value +  "),\n")
                        print("\n deq recvd=",deq) 
                        f_write.write(comma + "\n\t And("+  temp  + guards +  deq+ ")")
                        comma = ',' 
                        temp = ''
            f_write.write( "\n ) ) \n")

    f_write.write("\ndef main(k = 3):\n")
    for x in range(0,nbits) :
        f_write.write("\n   "+abcd[x]+" = [Bool('" + abcd[x]+"_%s' % i) for i in range(k+1)]")
    f_write.write("\n")
    for key,value in vard.items():
        print("\n   Vard :- Key: Value",key, value)
        g_var = key
    f_write.write("\n   "  + g_var +"="+"[Real("+"'"+g_var+ "_%s'" + " %i) for i in range(k+1)]")
    for key,value in inpd.items():
        print("\n   inpd :- Key: Value",key, value)
        in_var = key
        f_write.write("\n   "  + in_var +"="+"[Bool("+"'"+in_var+ "_%s'" + " %i) for i in range(k+1)]")



    f_write.write("\n   s = Solver()")
    f_write.write("\n   s.add(d<1)")
    f_write.write("\n   s.add(d>0)")
    f_write.write("\n # Add the initial state of the gate")
    f_write.write("\n   s.add(And(Not(a[0]), Not(b[0]), x[0] == 10))")
    f_write.write("\n   for i in range(1, k+1):")
    f_write.write("\n       s.add(Or(g1(i,a,b, d ,UP, DOWN,x),top(i,a,b, d ,UP, DOWN,x),g2(i,a,b, d ,UP, DOWN,x),bot(i,a,b, d ,UP, DOWN,x)))" ) 

    f_write.write("\n   print(s.sexpr())")
    f_write.write("\n   res = s.check()")
    f_write.write("\n   if res == sat:")
    f_write.write("\n\t     print(s.model())")
    f_write.write("\nif __name__ == '__main__':")
    f_write.write("\n\tmain(k=3)")
                       
    # This will be the connection between HAs
    return (s, d, ret_dict)


if __name__ == '__main__':
    import sys
    if len(sys.argv) < 2:
        print('Please give a file name')
        sys.exit(1)
    s = gen_smt(f=sys.argv[1], k=2)
    #if len(sys.argv) == 3:
       #print(s[0].sexpr())

