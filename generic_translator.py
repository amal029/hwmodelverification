#!/usr/bin/env python3
import networkx as nx
import sys
from networkx.drawing.nx_pydot import read_dot
# from sys import *
# from math import ceil
# import time
maxtime = 60


def bit_length(n):
    bits = 0
    while n >> bits:
        bits += 1
    return bits


def bit_check(no, bit_position):
    if (no >> bit_position & 1):
        return ""
    else:
        return "Not"


temp = ''
abcd = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', "i", "j", "k", "l", "m", "n"]


def dfs_iter(graph, start, path=[]):
    f = open('z3file.py', 'w')
    """
    Iterative version of depth first search.
    Arguments:
        graph - a dictionary of lists that is your \
                graph and who you're connected to.
        start - the node you wish to start at
        path - a list of already visited nodes for a path
    Returns:
        path - a list of strings that equal a valid path in the graph
    """
    f.write("from z3 import *\n")
    f.write("from sys import *\n")
    f.write("\nfrom math import *")
    f.write("\nimport networkx as nx ")

    q = [start]
    while q:
        v = q.pop()
        if v not in path:
            path += [v]
            m = len(path)
            q += graph[v]

    m = bit_length(len(path))
    temp = ''
    label = ''

    for i in range(len(path)):
        for x in range(0, m):
            temp += abcd[x]+","
        print("def ", path[i], "(", temp, "i,edge_data):")
        f.write("\ndef " + path[i] + "(" + temp + "i,edge_data):")
        f.write("\n   return Or(")
        temp = ''
        for j in range(len(path)):
            if nx.has_path(graph, path[i], path[j]):
                edge_data = graph.get_edge_data(path[i], path[j])
#                edge_data = edge_data.split(":")
                if edge_data is not None:
                    for k, v in edge_data.iteritems():
                        # print k , v ,"dict items"
                        label = v  # get the label only
                    print("State ", path[i], " has a  edge to", path[j],
                          "with edge label", edge_data)
                    for x in range(m):
                        temp += bit_check(i, m-x-1) + "(" + abcd[x] + "[i-1]),"
                    for x in range(m):
                        temp += bit_check(j, m-x-1) + "(" + abcd[x] + "[i]),"
                    f.write("\n   And(" + temp +
                            "edge_data ==" + label + "),")
                    print(temp)
                    temp = ""
        f.write(")\n")

    print("path m = ", path, m)
    f.write("\ndef main(k = 3):")
    for x in range(0, m):
        f.write("\n " + abcd[x] + " = [Bool('" + abcd[x] +
                "_%s' % i) for i in range(k+1)]")
    f.write("\n")
    f.write("\n edge_data =[Int('edge_data_%s' % i) for i in range(k+1)]")

    f.write("\n s = Solver()")
    f.write("\n for i in range(1, k+1):")
    for i in range(len(path)):
        for x in range(0, m):
            temp += abcd[x] + ","
        f.write("\n     s.add( " + path[i] + "(" +
                temp + "i,edge_data))")
        temp = ''

    f.write("\n print(s.sexpr())")
    f.write("\n res = s.check()")
    f.write("\n if res == sat:")
    f.write("\n    print(s.model())")
    f.write("\nif __name__ == '__main__':")
    f.write("\n     main(k=3)")

    return path


if __name__ == '__main__':
    print(sys.argv)
    graph = nx.DiGraph(read_dot('example.dot'))
    print(dfs_iter(graph, 'LR_0'))
