from z3 import *
from sys import *

from math import *
import networkx as nx 
ff, fd ,d = Real('ff'), Real('fd'),Real('d')
def top(i,a,b, d ,UP, DOWN,x): 
     return(Or( 

	 And( Not(a[i-1]),Not(b[i-1]),(a[i]),(b[i]),DOWN[i] , Not(UP[i]),x[i]==x[i-1],x[i] ==x[i-1] +( (1.0-x[i-1])/2.0 )*d )
 ) ) 

def bot(i,a,b, d ,UP, DOWN,x): 
     return(Or( 

	 And( Not(a[i-1]),(b[i-1]),(a[i]),Not(b[i]),UP[i] , Not(DOWN[i]),x[i]==x[i-1],x[i] ==x[i-1] +( ff - (x[i-1]/2.0) )*d )
 ) ) 

def g1(i, a,b, d ,UP, DOWN,x): 
     return(Or( 

	 And( (a[i-1]),Not(b[i-1]),Not(a[i]),Not(b[i]),x == 10.0 , Not(DOWN[i]) , Not(UP[i]),x[i]==x[i-1],),
	 And( (a[i-1]),Not(b[i-1]),(a[i]),(b[i]),DOWN[i] , Not(UP[i]),x[i]==x[i-1],x[i] ==x[i-1] +( (1.0-x[i-1])/2.0 )*d )
 ) ) 

def g2(i,a,b, d ,UP, DOWN,x): 
     return(Or( 

	 And( (a[i-1]),(b[i-1]),Not(a[i]),(b[i]),x == 1.0 , Not(UP[i]) , Not(DOWN[i]),x[i]==x[i-1],x[i] ==x[i-1] +( 0.0 )*d ),
	 And( (a[i-1]),(b[i-1]),(a[i]),Not(b[i]),UP[i] , Not(DOWN[i]),x[i]==x[i-1],x[i] ==x[i-1] +( ff - (x[i-1]/2.0) )*d )
 ) ) 

def main(k = 3):

   a = [Bool('a_%s' % i) for i in range(k+1)]
   b = [Bool('b_%s' % i) for i in range(k+1)]

   x=[Real('x_%s' %i) for i in range(k+1)]
   UP=[Bool('UP_%s' %i) for i in range(k+1)]
   DOWN=[Bool('DOWN_%s' %i) for i in range(k+1)]
   s = Solver()
   s.add(d<1)
   s.add(d>0)
 # Add the initial state of the gate
   s.add(And(Not(a[0]), Not(b[0]), x[0] == 10))
   for i in range(1, k+1):
       s.add(Or(g1(i,a,b, d ,UP, DOWN,x),top(i,a,b, d ,UP, DOWN,x),g2(i,a,b, d ,UP, DOWN,x),bot(i,a,b, d ,UP, DOWN,x)))
   print(s.sexpr())
   res = s.check()
   if res == sat:
	     print(s.model())
if __name__ == '__main__':
	main(k=3)

