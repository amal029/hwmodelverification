#!/usr/bin/env python3

from pydotplus.graphviz import graph_from_dot_file
from z3 import Real, Bool, SolverFor, parse_smt2_string
from z3 import Not, And, Or

import math
from expressions import Parser


def getattr(sg, tosearch, n):
    try:
        return n.obj_dict['attributes'][tosearch].strip('"').strip()
    except KeyError:
        return None


def dicts(sg, tosearch, k):
    ret = dict()
    try:
        for v in sg.obj_dict['attributes'][tosearch].split(','):
            vars = list()
            v = v.strip('"').strip()
            vars += [Real(v+'_%s' % i)
                     for i in range(k+1)]
            ret[v] = vars
    except KeyError:
        pass

    return ret


def encode_state_expr(bvec, index, bvars):
    ret = []
    for i, b in enumerate(bvec):
        g = bvars[i][index]
        ret += [Not(g)] if b == '0' else [g]

    return(And(*ret))


def gen_smt(f=None, k=10, TIMEOUT=1000):
    # Initialize the parser
    parser = Parser()

    # initialize the solver
    s = SolverFor('QF_NRA')

    # Now input the δ
    d = Real('d')
    s.add(d > 0)

    # Now add the smt for each HA
    G = graph_from_dot_file(f)
    ret_dict = {}
    for sg in G.get_subgraphs():
        # print(sg.get_attributes())
        ha_name = sg.obj_dict['attributes']['label'].strip('"')
        # print(ha_name)
        # Each subgraph is an HA
        vard, ind, outd, alld, tuned = {}, {}, {}, {}, {}

        try:
            for v in sg.obj_dict['attributes']['tunables'].split(','):
                v = v.strip('"').strip()
                tuned.update({str(v): [(Real(v))]})
        except KeyError:
            pass

        # The internal continuous vars
        vard = dicts(sg, 'vars', k)
        # print(vard)

        # Now the inputs
        ind = dicts(sg, 'inputs', k)
        # print(ind)

        # Now the outputs
        outd = dicts(sg, 'outputs', k)
        # print(outd)

        # The dictionary with all dicts
        alld.update(vard)
        alld.update(outd)
        alld.update(ind)
        alld.update(tuned)

        # Make the decls for converting
        decls = {'d': d}
        for i in alld.values():
            decls.update({str(ii): ii
                          for ii in i})
        # print(decls)

        # Now add the initial conditions to the solver
        try:
            init_v = ('(assert ' +
                      parser.input_parse(
                          sg.obj_dict['attributes']['initial'].strip('"'),
                          rindex='0', lindex=0, vars=vard, tunes=tuned) + ')')
            # Now add the initial for outputs
            s.add(parse_smt2_string(init_v, decls=decls))
            # print(init_v)
        except KeyError:
            pass
        try:
            init_o = ('(assert ' +
                      parser.input_parse(
                          sg.obj_dict['attributes']['initial_output'].strip('\
                          "'),
                          rindex=0, lindex=0, vars=alld, tunes=tuned) + ')')
            s.add(parse_smt2_string(init_o, decls=decls))
        except KeyError:
            pass

        # Now encode the states as binary vectors

        # TODO: Get the number of bits needed to represent the states in
        # the HA.
        initial_loc = sg.obj_dict['attributes']['initial_loc'].strip('"')
        initial_loc.strip()
        nodes = sg.get_nodes()
        nbits = math.ceil(math.log2(len(nodes))) if len(nodes) > 1 else 1
        # Initialize the variables for state encoding
        nbitvars = [[Bool(ha_name+'_'+str(i)+'_%s' % j)
                     for j in range(k+1)]
                    for i in range(nbits)]
        fstring = "{0:0"+str(nbits)+"b}"
        svec = {n.get_name():
                [encode_state_expr(fstring.format(i), j, nbitvars)
                 for j in range(k+1)]
                for i, n in enumerate(nodes)}
        # print('tutu:', svec)
        s.add(svec[initial_loc][0])

        # TODO: Now encode the HA
        edges = sg.get_edges()
        for i in range(1, k+1):
            node_ors = []
            for n in nodes:
                # print(n.get_name(), n.get_attributes())
                # TODO: first get all the connected edges with this as the
                # source
                n_dests = [(e.get_destination(),
                            e.obj_dict['attributes']['guard'].strip('"'),
                            e.obj_dict['attributes']['updates'].strip('"'))
                           for e in edges
                           if e.get_source() == n.get_name()]
                # print(n.get_name(), '-->', n_dests)
                # Make the transitions
                n_ders = getattr(sg, 'odes', n)
                n_invs = getattr(sg, 'invariant', n)
                n_eqs = getattr(sg, 'eqs', n)

                der = True
                if n_ders is not None:
                    der = parser.input_parse(n_ders, lindex=i, rindex=i-1,
                                             vars=vard, tunes=tuned)
                    der = parse_smt2_string('(assert '+der+')', decls=decls)
                inv = True
                if n_invs is not None:
                    inv = parser.input_parse(n_invs, lindex=i-1,
                                             rindex=i-1,
                                             vars=alld, tunes=tuned)
                    inv = parse_smt2_string('(assert '+inv+')', decls=decls)

                eqs = True
                if n_eqs is not None:
                    eqs = parser.input_parse(n_eqs, lindex=i,
                                             rindex=i-1,
                                             vars=alld, tunes=tuned)
                    eqs = parse_smt2_string('(assert '+eqs+')', decls=decls)
                sand = And(svec[n.get_name()][i-1],
                           svec[n.get_name()][i],
                           inv, der, eqs)
                # print(sand)
                # TODO: Now all the other outgoing edges
                # print(alld)
                oand = [And(
                    svec[n.get_name()][i-1],
                    svec[n_name][i],
                    parse_smt2_string('(assert ' +
                                      parser.input_parse(guard, lindex=i-1,
                                                         rindex=i-1,
                                                         vars=alld,
                                                         tunes=tuned) + ')',
                                      decls=decls),
                    parse_smt2_string('(assert ' +
                                      parser.input_parse(update,
                                                         lindex=i,
                                                         rindex=i-1,
                                                         vars=alld,
                                                         tunes=tuned) + ')',
                                      decls=decls)
                ) for (n_name, guard, update) in n_dests]
                # print(oand)
                # Now just Or sand and oands
                node_ors.append(Or(*(oand + [sand])))
            # TODO:  Now just or the node_ors
            s.add(Or(*node_ors))
        ret_dict[sg.get_name()] = {'vars': vard, 'inputs': ind,
                                   'outputs': outd, 'tunables': tuned,
                                   'states': svec}
    # This will be the connection between HAs
    return (s, d, ret_dict)


if __name__ == '__main__':
    import sys
    if len(sys.argv) < 2:
        print('Please give a file name')
        sys.exit(1)
    s = gen_smt(f=sys.argv[1], k=2)
    if len(sys.argv) == 3:
        print(s[0].sexpr())
