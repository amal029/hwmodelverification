#!/usr/bin/env python3
from lark import Lark, Transformer
from z3 import Real, SolverFor
from z3 import parse_smt2_string


action_grammar = """
    ?starta: NAME "=" sum    -> action
          | starta "," starta -> actions
    ?sum: product
        | sum "+" product   -> add
        | sum "-" product   -> sub
    ?product: atom
        | product "*" atom  -> mul
        | product "/" atom  -> div
    ?atom: NUMBER           -> number
         | "-" atom         -> neg
         | NAME             -> var
         | "(" sum ")"
    %import common.CNAME -> NAME
    %import common.NUMBER
    %import common.WS_INLINE
    %ignore WS_INLINE
"""

ode_grammar = """
    ?der: "der" "(" NAME ")" "=" sum
        | der "," der -> ders
""" + action_grammar

guard_grammar = """
    ?startg: logic
           | startg "&&" startg -> andd
           | startg "||" startg -> orr
           | "(" startg ")"

    ?logic: NAME             -> lname
           |NAME ">" sum     -> gt
           |NAME "<" sum     -> lt
           |NAME ">=" sum    -> geq
           |NAME "<=" sum    -> leq
           |NAME "!=" sum    -> neq
           |NAME "==" sum    -> eq
""" + action_grammar


class ActionTransformer(Transformer):
    def __init__(self, lindex, rindex, vars, tunes):
        self.lindex = lindex
        self.rindex = rindex
        self.vars = vars
        self.tunes = tunes

    def actions(self, action):
        # self.solver.add(And(*action))
        return '(and ' + ' '.join(action) + ')'
        # return 'And(' + ret + ')'

    def action(self, name):
        return ('(= ' + str(self.vars[name[0]][self.lindex]) + ' '
                + str(name[1]) + ')')
        # return name[0] + '[i] == (' + str(name[1]) + ')'

    def div(self, input):
        l = input[0]
        r = input[1]
        return ('(/ '+str(l)+' '+str(r)+')')

    def mul(self, input):
        l = input[0]
        r = input[1]
        if type(l) != str and type(r) != str:
            return (l * r)
        else:
            return ('(* '+str(l)+' '+str(r)+')')
            # return (str(l)+' * ' + str(r))

    def sub(self, input):
        l = input[0]
        r = input[1]
        if type(l) != str and type(r) != str:
            return (l - r)
        else:
            return ('(- '+str(l)+' '+str(r)+')')

    def add(self, input):
        l = input[0]
        r = input[1]
        if type(l) != str and type(r) != str:
            return (l + r)
        else:
            return ('(+ '+str(l)+' '+str(r)+')')
            # return (str(l)+' + ' + str(r))

    def number(self, n):
        return str(n[0])

    def neg(self, n):
        n = n[0]
        if type(n) == str:
            return ('-'+n+'')
        else:
            return (-n)

    def var(self, n):
        try:
            ret = str(self.vars[n[0]][self.rindex])
        except Exception:
            ret = str(self.tunes[n[0]][0])
        return ret


class GuardTransformer(ActionTransformer):

    def lname(self, action):
        ret = ('(= ' + str(self.vars[action[0]][self.lindex]) + ' 1.0)')
        return ret

    def orr(self, actions):
        return ('(or '+str(actions[0])+' '+actions[1]+')')
        # return ('Or(' + ','.join(actions) + ')')

    def andd(self, actions):
        return ('(and '+str(actions[0])+' '+str(actions[1])+')')
        # return ('And(' + ','.join(actions) + ')')

    def gt(self, action):
        l = self.vars[action[0]][self.lindex]
        return ('(> '+str(l)+' '+str(action[1])+')')

    def lt(self, action):
        l = self.vars[action[0]][self.lindex]
        # r = action[1]
        return ('(< '+str(l)+' '+str(action[1])+')')
        # return (l+'[i-1] < (' + str(r) + ')')

    def geq(self, action):
        l = self.vars[action[0]][self.lindex]
        # r = action[1]
        return ('(>= '+str(l)+' '+str(action[1])+')')
        # return (l+'[i-1] >= (' + str(r) + ')')

    def leq(self, action):
        l = self.vars[action[0]][self.lindex]
        return ('(<= '+str(l)+' '+str(action[1])+')')

    def eq(self, action):
        l = self.vars[action[0]][self.lindex]
        return ('(= '+str(l)+' '+str(action[1])+')')

    def neq(self, action):
        l = self.vars[action[0]][self.lindex]
        return ('(not (= '+str(l)+' '+str(action[1])+'))')


class OdeTransformer(ActionTransformer):
    def der(self, der):
        varl = str(self.vars[der[0]][self.lindex])
        varr = str(self.vars[der[0]][self.rindex])
        slope = str(der[1])
        return ('(= ' + varl + ' (+ ' + varr + ' (* d '
                + slope + ')))')

    def ders(self, ders):
        ret = ' '.join(ders)
        return ('(and ' + ret + ')')


class Parser:

    def __init__(self, lindex=None, rindex=None, vars=None, tunes=None):
        self.at = ActionTransformer(lindex, rindex, vars, tunes)
        self.ot = OdeTransformer(lindex, rindex, vars, tunes)
        self.gt = GuardTransformer(lindex, rindex, vars, tunes)

        self.actions = Lark(action_grammar, parser='lalr',
                            start='starta', transformer=self.at).parse
        self.odes = Lark(ode_grammar, parser='lalr',
                         start='der', transformer=self.ot).parse
        self.guards = Lark(guard_grammar, parser='lalr',
                           start='startg',
                           transformer=self.gt).parse

    def input_parse(self, arg, lindex=None, rindex=None, vars=None,
                    tunes=None):
        self.at.lindex = lindex
        self.ot.lindex = lindex
        self.gt.lindex = lindex
        self.at.rindex = rindex
        self.ot.rindex = rindex
        self.gt.rindex = rindex
        self.at.vars = vars
        self.ot.vars = vars
        self.gt.vars = vars
        self.at.tunes = tunes
        self.ot.tunes = tunes
        self.gt.tunes = tunes
        # Try to parse the input string one after another.
        ret = None
        try:
            ret = self.actions(arg)
        except Exception as e1:
            try:
                ret = self.guards(arg)
            except Exception as e2:
                try:
                    ret = self.odes(arg)
                except Exception as e3:
                    raise RuntimeError('Cannot parse string: ', arg, '\n',
                                       e1, '\n', e2, '\n', e3)
        return ret


# Tests
if __name__ == '__main__':
    parser = Parser()
    print('\n')
    # Example of actions
    s = SolverFor('QF_NRA')
    vars = [Real('a_%s' % i) for i in range(2)]
    vars += [Real('b_%s' % i) for i in range(2)]
    vard = {'a': [Real('a_%s' % i) for i in range(2)],
            'b': [Real('b_%s' % i) for i in range(2)]}
    s.add(*[v == v for v in vars])
    d = Real('d')
    s.add(d > 0)
    # ret = input_parse('a = 4/8', vars=vard, lindex=0)
    # print(ret)
    # ret = ''.join([s.sexpr(), ret])
    # print(ret)
    # s.add(parse_smt2_string(ret))
    # s.simplify()
    # print(s.sexpr())
    ret = []
    for i in range(1, 2):
        ret += ['(assert ' +
                parser.input_parse('a = 4/8, a = 5-(90+5*-4), a = b',
                                   vars=vard, lindex=i, rindex=i-1) +
                ')']
    # Example of derivatives
    for i in range(1, 2):
        ret += ['(assert ' +
                parser.input_parse(
                    'der(a) = 4/8, der(a) = a - 5 - (90+(5*-4))',
                    vars=vard, lindex=i, rindex=i-1) + ')']
    # Example of guards
    for i in range(1, 2):
        ret += ['(assert' +
                parser.input_parse(
                    'a == 4/9 && (a != 5-(90+5*-4)) || (b == a + 7)',
                    vars=vard, lindex=i, rindex=i-1) + ')']
        ret += ['(assert' +
                parser.input_parse(
                    'a >= 4/9 && (a <= 5-(90+5*-4)) || (b < a + 7/8)',
                    vars=vard, lindex=i, rindex=i-1) + ')']

    ret = '\n'.join(ret)
    ret = ''.join([s.sexpr(), ret])
    # print(ret)
    s.add(parse_smt2_string(ret))
    print(s.sexpr())
