#!/usr/bin/env python3

from z3 import Or, And, sat
from gensmt import gen_smt as smt


def main(f=None, k=1):
    s, d, rdict = smt(f=f, k=k)
    vars = rdict['BB']['vars']
    # ins = rdict['BB'][1]
    # outs = rdict['BB'][2]
    s.add(d < 1)
    # A property to force it to move
    s.add(Or(*[vars['h'][i] == 0 for i in range(k+1)]))
    # Safety property
    s.add(And(*[vars['h'][i] >= 0 for i in range(k+1)]))
    # print(s.sexpr())
    ret = s.check()
    if ret == sat:
        # print(s.model())
        print(s.model()[d])
        for i in range(k+1):
            print(s.model()[vars['t'][i]].as_decimal(9),
                  ',', s.model()[vars['h'][i]].as_decimal(9),
                  ',', s.model()[vars['v'][i]].as_decimal(9))
    print(ret)


main(f='bb.dot', k=7)
