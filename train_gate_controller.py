#!/usr/bin/env python3

from z3 import Or, And, Not, sat
from gensmt import gen_smt as smt


def main(f=None, k=1):
    s, d, rdict = smt(f=f, k=k)
    s.add(d < 1)

    # print(s.sexpr())
    # This is enforcing liveness
    s.add(Or(*[And(rdict['Controller']['states']['c4'][i])
               for i in range(k+1)]))
    # Another property that the gate actually goes down
    s.add(Or(*[And(rdict['Gate']['vars']['x'][i] <= 2,
                   rdict['Train']['vars']['y'][i] >= 5,
                   rdict['Train']['vars']['y'][i] <= 15)
               for i in range(k+1)]))

    # This is enforcing safety
    s.add(And(*[Not(And(rdict['Train']['vars']['y'][i] == 0,
                        rdict['Gate']['vars']['x'][i] < 10))
                for i in range(k+1)]))

    # Print the sexpression
    # print(s.sexpr())

    ret = s.check()
    print(ret)
    if (ret == sat):
        print('d:', s.model()[d])
        print('v:', s.model()[rdict['Gate']['tunables']['ff'][0]])
        print('bb:', s.model()[rdict['Train']['tunables']['bb'][0]])
        print('DOWN,UP,y,x,t')
        for i in range(k+1):
            print(s.model()[rdict['Controller']['outputs']['DOWN'][i]], ',',
                  s.model()[rdict['Controller']['outputs']['UP'][i]], ',',
                  s.model()[rdict['Train']['vars']['y'][i]], ',',
                  s.model()[rdict['Gate']['vars']['x'][i]], ',',
                  s.model()[rdict['Train']['vars']['t'][i]])


main(f='train_gate_controller.dot', k=35)
